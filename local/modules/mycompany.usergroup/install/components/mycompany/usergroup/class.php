<?php
namespace MyCompany\Test\Components;

use Bitrix\Main,
    Bitrix\Main\Localization,
    Bitrix\Main\Loader;
use MyCompany\Test;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

class UserGroup extends \CBitrixComponent
{
    
    public function onPrepareComponentParams($arParams)
    {

        self::tryParseInt($arParams["CACHE_TIME"], 3600, true);
        self::tryParseString($arParams["PAGE_TITLE"]);
        
        return $arParams;
    }
    
    public static function tryParseInt(&$fld, $default, $allowZero = null)
    {
        $fld = intval($fld);
        if(!$allowZero && !$fld && isset($default))
            $fld = $default;

        return $fld;
    }

    public static function tryParseString(&$fld, $default = null)
    {
        $fld = trim((string)$fld);
        if(!strlen($fld) && isset($default))
            $fld = htmlspecialcharsbx($default);

        return $fld;
    }

    public static function tryParseBoolean(&$fld)
    {
        $fld = $fld == 'Y';
        return $fld;
    }

	public function executeComponent()
	{


		$this->setFrameMode(false);

		$defaultUrlTemplates404 = [
			"index" => "index.php",
			"detail" => "#ID#/"
		];

        $variables = [];
        $componentVariables = ["ID"];
		$request = Main\Application::getInstance()->getContext()->getRequest();

		if ($this->arParams["SEF_MODE"] == "Y")
		{
			$templatesUrls = \CComponentEngine::makeComponentUrlTemplates($defaultUrlTemplates404, $this->arParams["SEF_URL_TEMPLATES"]);

			$variableAliases = \CComponentEngine::makeComponentVariableAliases([], $this->arParams["VARIABLE_ALIASES"]);

			$componentPage = \CComponentEngine::parseComponentPath(
				$this->arParams["SEF_FOLDER"],
				$templatesUrls,
				$variables
			);
            
            if(!$componentPage)
            {
                $componentPage = "index";
            }

			\CComponentEngine::initComponentVariables($componentPage, $componentVariables, $variableAliases, $variables);

			$this->arResult = array_merge(
				[
					"SEF_FOLDER" => $this->arParams["SEF_FOLDER"],
					"URL_TEMPLATES" => $templatesUrls,
					"VARIABLES" => $variables,
					"ALIASES" => $variableAliases,
				],
				$this->arResult
			);
		}
		else
		{
			$variableAliases = \CComponentEngine::makeComponentVariableAliases([], $this->arParams["VARIABLE_ALIASES"]);
			\CComponentEngine::initComponentVariables(false, $componentVariables, $variableAliases, $variables);

			if ($request->get('ID'))
			{
				
				$componentPage = "detail";
				
			}

			if (empty($componentPage))
			{
				
				$componentPage = "index";
			}

			$currentPage = $request->getRequestedPage();

			$this->arResult = [
				"VARIABLES" => $variables,
				"ALIASES" => $variableAliases,
				"SEF_FOLDER" => $currentPage,
				""
			];

		}

		if ($componentPage == "index" && $this->getTemplateName() !== "")
			$componentPage = "template";

		$this->includeComponentTemplate($componentPage);
	}
}