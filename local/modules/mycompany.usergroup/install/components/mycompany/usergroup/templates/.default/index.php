<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

?>

<?$APPLICATION->IncludeComponent(
    "mycompany:usergroup.list",
    "",
    Array(
        "CACHE_TIME" => $arParams['CACHE_TIME'],
        "CACHE_TYPE" => $arParams['CACHE_TYPE'],
        "PAGE_TITLE" => $arParams['PAGE_TITLE']
    ),
    $component
);?>