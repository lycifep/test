<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) { die(); }

use Bitrix\Main\Localization\Loc;

$arComponentDescription = [
	'NAME' => Loc::getMessage('MYCOMPANY_USERGROUP_DESCRIPTION_TITLE'),
    "DESCRIPTION" => Loc::getMessage("MYCOMPANY_USERGROUP_DESCRIPTION_DESCRIPTION"),
    "CACHE_PATH" => "Y",
    "COMPLEX" => "Y",
    "PATH" => [
        "ID" => "mycompany_components",
        "NAME" => Loc::getMessage("MYCOMPANY_USERGROUP_MAIN_FOLDER_NAME")
    ]
];