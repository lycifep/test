<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = [
	"PARAMETERS" => [

		"PAGE_TITLE" => [
            "PARENT" => "BASE",
            "NAME" => Loc::getMessage('MYCOMPANY_USERGROUP_LIST_PARAMETERS_PAGE_TITLE'),
            "TYPE" => "STRING"
        ],
        
        "CACHE_TIME"  =>  ["DEFAULT"=>3600],

		"SEF_MODE" => [
            "index" => [
                "NAME" => 'Список групп',
                "DEFAULT" => "index.php",
                "VARIABLES" => []
            ],
            "detail" => [
                "NAME" => 'Подробная информация группы',
                "DEFAULT" => "#ID#/",
                "VARIABLES" => ["ID"]
            ],
        ]
	]
];

?>