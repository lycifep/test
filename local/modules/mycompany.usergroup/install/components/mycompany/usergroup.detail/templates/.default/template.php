<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL'])){
    foreach($arResult['ERRORS']['FATAL'] as $error){
        ShowError($error);
    }

} elseif(!empty($arResult['ERRORS']['NONFATAL'])) {
    foreach($arResult['ERRORS']['NONFATAL'] as $error){
        ShowError($error);
    }
}
?>

<?
if(!empty($arResult['DATA'])) {?>
    
    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th><?=Loc::getMessage('TABLE_HEAD_NAME')?></th>
                <th><?=Loc::getMessage('TABLE_HEAD_DESCRIPTION')?></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><?=$arResult['DATA']['ID']?></td>
                <td><?=$arResult['DATA']['NAME']?></td>
                <td><?=$arResult['DATA']['DESCRIPTION']?></td>
            </tr>      
        </tbody>
    </table>

<?} else {?>

Ничего не найдено!

<?}?>