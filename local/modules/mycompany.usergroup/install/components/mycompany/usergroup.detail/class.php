<?php

namespace MyCompany\Test\Components;

use Bitrix\Main,
    Bitrix\Main\Localization,
    Bitrix\Main\Loader;
use MyCompany\Test;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

class UserGroupList extends \CBitrixComponent
{
	protected $errorsFatal = [];
    protected $errorsNonFatal = [];
    
    protected $dbResult = [];

    public function __construct($component = null){
        
        parent::__construct($component);

        Localization\Loc::loadMessages(__FILE__);
    }
    
	protected function checkRequiredModules() {
        
    }
    
    public function onPrepareComponentParams($arParams)
	{

        self::tryParseInt($arParams["CACHE_TIME"], 3600, true);
        self::tryParseInt($arParams["ID"], 0);
        
		return $arParams;
	}
    
    public static function tryParseInt(&$fld, $default, $allowZero = null)
    {
        $fld = intval($fld);
        if(!$allowZero && !$fld && isset($default))
            $fld = $default;

        return $fld;
    }

    public static function tryParseString(&$fld, $default = null)
    {
        $fld = trim((string)$fld);
        if(!strlen($fld) && isset($default))
            $fld = htmlspecialcharsbx($default);

        return $fld;
    }

    public static function tryParseBoolean(&$fld)
    {
        $fld = $fld == 'Y';
        return $fld;
    }

    protected function formatResultErrors(){
        
        $errors = [];
        if (!empty($this->errorsFatal))
            $errors['FATAL'] = $this->errorsFatal;
        if (!empty($this->errorsNonFatal))
            $errors['NONFATAL'] = $this->errorsNonFatal;

        if (!empty($errors))
            $this->arResult['ERRORS'] = $errors;

    }
    
	protected function obtainData(){
        
        
        $res = Main\GroupTable::getList([
            'select' => ['ID', 'NAME', 'DESCRIPTION'],
            'filter' => ['ID' => $this->arParams["ID"]]
        ]);
        if($arRes = $res->fetch()) {
            $this->dbResult = $arRes;
        } else {

            $this->abortResultCache();
        }       
    }
    
    protected function getFormatResultGroup($group){
        
        $group['DESCRIPTION'] = htmlspecialcharsbx($group['DESCRIPTION']);

        return $group;
    }
    
    protected function formatResult(){
        
        if(!empty($this->dbResult))
            $this->arResult['DATA'] = $this->getFormatResultGroup($this->dbResult);    
        
    }
    
    public function executeComponent(){
        
        if($this->startResultCache()) {
        
            try {
                
                $this->setFrameMode(true);
                $this->checkRequiredModules();
                $this->obtainData();            
                $this->formatResult();
            
            } catch (Exception $e){
                
                $this->abortResultCache();
                $this->errorsFatal[$e->getCode()] = $e->getMessage();
            }

            $this->formatResultErrors();

            $this->includeComponentTemplate();
        }     
    }
    
}