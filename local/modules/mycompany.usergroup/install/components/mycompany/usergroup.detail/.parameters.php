<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Bitrix\Main\Localization\Loc;

$arComponentParameters = [
	"PARAMETERS" => [

		"ID" => [
            "PARENT" => "BASE",
            "NAME" => 'ID',
            "TYPE" => "STRING"
        ],

		"CACHE_TIME"  =>  ["DEFAULT"=>3600]
	]
];

?>