<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
use Bitrix\Main,
    Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL'])){
    foreach($arResult['ERRORS']['FATAL'] as $error){
        ShowError($error);
    }

} elseif(!empty($arResult['ERRORS']['NONFATAL'])) {
    foreach($arResult['ERRORS']['NONFATAL'] as $error){
        ShowError($error);
    }
}
?>

<?
if(count($arResult['GROUPS']) > 0) {?>
    
    <table class="table">
        <caption><?=Loc::getMessage('TABLE_CAPTION')?></caption>
        <thead>
            <tr>
                <th>ID</th>
                <th><?=Loc::getMessage('TABLE_HEAD_NAME')?></th>
                <th><?=Loc::getMessage('TABLE_HEAD_DESCRIPTION')?></th>
            </tr>
        </thead>
        <tbody>
        <?
        $i = 0;
        foreach($arResult['GROUPS'] as $group){?>
            <tr>
                <td><?=$group['ID']?></td>
                <td><?=$group['NAME']?></td>
                <td><?=$group['DESCRIPTION']?></td>
            </tr>      
        <?
            $i++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="3">Кол-во: <?=$i?></th>
            </tr>
        </tfoot>
    </table>

<?}?>