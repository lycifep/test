<?
use Bitrix\Main\Localization\Loc;

Class mycompany_usergroup extends CModule {
        
    var $MODULE_ID = "mycompany.usergroup";

    public function __construct() {
        
        $arModuleVersion = null;
        include __DIR__ . '/version.php';
        if (isset($arModuleVersion) && is_array($arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_NAME = 'Группы пользователей';
        $this->MODULE_DESCRIPTION = 'Модуль для работы с групами пользователей';
        $this->PARTNER_NAME = 'Компания MyCompany';
        $this->PARTNER_URI = 'https://mycompany.com/';
    }

    function DoInstall() {
        
        $this->InstallFiles();
        
        RegisterModule($this->MODULE_ID);
        
        
    }

    function DoUninstall() {
        
        $this->UnInstallFiles();        
        
        UnRegisterModule($this->MODULE_ID);
    }
    
    function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        
        CopyDirFiles(
            __DIR__ . '/components',
            $_SERVER['DOCUMENT_ROOT'].'/bitrix/components',
            true,
            true
        );
       
    }
    
    function UnInstallFiles(){
        
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        
        DeleteDirFilesEx('bitrix/components/mycompany');
    }
    
}

?>

