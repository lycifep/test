<?
use Bitrix\Main;
use Bitrix\Main\Localization\Loc;

Class mycompany_currate extends CModule {
        
    var $MODULE_ID = "mycompany.currate";
    var $errors;

    public function __construct() {
        
        $arModuleVersion = null;
        include __DIR__ . '/version.php';
        if (isset($arModuleVersion) && is_array($arModuleVersion))
        {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }
        
        $this->MODULE_NAME = 'Курсы валют';
        $this->MODULE_DESCRIPTION = 'Модуль для работы с курсами валют';
        $this->PARTNER_NAME = 'Компания MyCompany';
        $this->PARTNER_URI = 'https://mycompany.com/';
    }

    function DoInstall() {
        
        RegisterModule($this->MODULE_ID);
        
        $this->InstallDB();
        $this->InstallFiles();
        $this->CreateAgents();
        
        Main\EventManager::getInstance()->registerEventHandler(
            "main",
            "OnBuildGlobalMenu",
            $this->MODULE_ID,
            "\\MyCompany\\Currate\\Events",
            "OnBuildGlobalMenuHandlerMyCompany"
        );
    }

    function DoUninstall() {
        
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->DeleteAgents();        
        
        UnRegisterModule($this->MODULE_ID);
        

        Main\EventManager::getInstance()->unRegisterEventHandler(
            "main",
            "OnBuildGlobalMenu",
            $this->MODULE_ID,
            "\\MyCompany\\Currate\\Events",
            "OnBuildGlobalMenuHandlerMyCompany"
        );
    }
    
    function DeleteAgents() {
 
        \CAgent::RemoveAgent("\\MyCompany\\Currate\\Agent::getCurrencyRate();", $this->MODULE_ID);
        
    }
    
    function CreateAgents() {
        
        \CAgent::AddAgent(
            "\\MyCompany\\Currate\\Agent::getCurrencyRate();",
            $this->MODULE_ID,
            "N",
            86400
        );

    }
    
    function InstallDB()
    {
        global $DB, $DBType, $APPLICATION;
        
        $this->errors = false;
        
        if(!$DB->query("SELECT 'x' FROM mycompany_currate", true))
        {
            $this->errors = $DB->RunSQLBatch(__DIR__ . "/db/".$DBType."/install.sql");
        }

        if($this->errors !== false)
        {
            $APPLICATION->ThrowException(implode("", $this->errors));
            return false;
        }

    }
    
    function InstallFiles()
    {
        CopyDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin', true, true);
        
        CopyDirFiles(
            __DIR__ . '/components',
            $_SERVER['DOCUMENT_ROOT'].'/bitrix/components',
            true,
            true
        );
       
    }
    
    function UnInstallDB($arParams = array())
    {
        global $DB, $DBType, $APPLICATION;
        
        $this->errors = false;
        $arSQLErrors = [];
        
        if(!array_key_exists("savedata", $arParams) || ($arParams["savedata"] != "Y"))
        {
            
            $this->errors = $DB->RunSQLBatch(__DIR__ . "/db/".$DBType."/uninstall.sql");
        }

        if(is_array($this->errors))
            $arSQLErrors = array_merge($arSQLErrors, $this->errors);

        if(!empty($arSQLErrors))
        {
            $this->errors = $arSQLErrors;
            $APPLICATION->ThrowException(implode("", $arSQLErrors));
            
            return false;
        }

    }
    
    function UnInstallFiles(){
        
        DeleteDirFiles(__DIR__ . '/admin', $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin');
        
        DeleteDirFilesEx('bitrix/components/mycompany');
    }
    
}

?>

