<?php

namespace MyCompany\Currate;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class CurrateTable extends Entity\DataManager
{
    public static function getFilePath()
    {
        return __FILE__;
    }

    public static function getTableName()
    {
        return 'mycompany_currate';
    }

    public static function getMap()
    {
        return array(
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ID'
                )
            ),
            new Entity\StringField('CODE', array(
                'required' => true,
                'title' => 'Код валюты',
                'validation' => function(){
                    return array(
                        new Entity\Validator\Length(null, 5),
                    );
                }
                )
            ),
            new Entity\DatetimeField('DATE', array(
                'required' => false,
                'title' => 'Время'
                )
            ),
            new Entity\FloatField('COURCE', array(
                'required' => false,
                'title' => 'Курс'
                )
            )
        );
    }
    
}