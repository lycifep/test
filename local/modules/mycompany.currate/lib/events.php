<?php
namespace MyCompany\Currate;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

class Events {
	
	public static function OnBuildGlobalMenuHandlerMyCompany(&$arGlobalMenu, &$arModuleMenu){
		
		$moduleID = 'mycompany_currate';

        $arMenu = [
            'menu_id' => 'global_menu_mc_currate',
            'text' => 'MC: курсы валют',
            'title' => 'MC: курсы валют',
            'sort' => 1000,
            'items_id' => 'global_menu_mc_currate_items',
            'items' => [
                [
                    'text' => 'Настройка',
                    'title' => 'Настройка',
                    'sort' => 10,
                    'url' => '/bitrix/admin/'.$moduleID.'_setting.php',
                    'items_id' => 'setting',
                ]
            ],
        ];

        $arGlobalMenu[] = $arMenu;
	}

}