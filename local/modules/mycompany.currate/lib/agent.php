<?php
/**
 * Bitrix Framework
 * @package    Bitrix
 * @subpackage mlife.smsservices
 * @copyright  2015 Zahalski Andrew
 */

namespace MyCompany\Currate;

use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\XML;

class Agent {
	
	public static function getCurrencyRate() {
        
        $moduleId = "mycompany.currate"; 
        
        $objDateTime = new \Bitrix\Main\Type\DateTime;
        $shortDateTime = $objDateTime->format("d.m.Y");
        
        $curListJson = \COption::GetOptionString($moduleId, "currency_code");
        $arCurList = json_decode($curListJson, true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            $arCurList = [];       
        }
        
        $lastDateImportJson = \COption::GetOptionString($moduleId, "last_date_import", '');
        $arLastImport = json_decode($lastDateImportJson, true);
        if(json_last_error() !== JSON_ERROR_NONE) {
            $arLastImport = [];       
        }
        
        $httpClient = new HttpClient(); 
        $httpClient->setHeader('Content-Type', 'application/xml; charset=UTF-8', true);
        $httpClient->query('GET', "https://www.cbr.ru/scripts/XML_daily.asp", "date_req=".$shortDateTime);

        $arSetLastDateImport = [];
        if ($response = $httpClient->getResult()){
            
            $xml = new \CDataXML();
            $xml->LoadString($response);
            $arData = $xml->GetArray();

            if (is_array($arData) && count($arData["ValCurs"]["#"]["Valute"])>0)
            {
                for ($i = 0; $i < count($arData["ValCurs"]["#"]["Valute"]); $i++)
                {
                    $currency = $arData["ValCurs"]["#"]["Valute"][$i]["#"]["CharCode"][0]["#"];
                    
                    if(!in_array($currency, $arCurList)) continue;
                    if(isset($arLastImport[$currency]) && $arLastImport[$currency]['date_import'] >= $shortDateTime) continue;
                    
                    $rate = DoubleVal(str_replace(",", ".", $arData["ValCurs"]["#"]["Valute"][$i]["#"]["Value"][0]["#"]));

                    \MyCompany\Currate\CurrateTable::add(['CODE' => $currency, 'COURCE' => $rate, 'DATE' => $objDateTime]);
                    $arSetLastDateImport[$currency]['date_import'] = $shortDateTime;
                }

            }
        }	
        
        if(!empty($arSetLastDateImport)) {
            
            \COption::SetOptionString($moduleId, "last_date_import", json_encode(array_merge($arCurList, $arSetLastDateImport), JSON_UNESCAPED_UNICODE));    
        }
        
        return '\\MyCompany\\Currate\\Agent::getCurrencyRate();';
	
	}
}